﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data;
using System.Data.SqlClient;
using System.Data.Sql;

namespace DAE_Avz_lab04_WPF
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        SqlConnection conn = new SqlConnection(@"Data source=LAPTOP-ROG002\SQLEXPRESS;initial catalog=db_lab04;User Id=usrOrtega;Password=12345");

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btnConsultar_Click(object sender, RoutedEventArgs e)
        {
            //ESCENARIO CONECTADO / PROCEDIMIENTOS ALMACENADOS
            List<Persona> personas = new List<Persona>();

            //abre la conexión
            conn.Open();

            //se crea un comando para hacer uso de procedimientos almacendos
            SqlCommand cmd = new SqlCommand("USP_BuscarPersonaNombre", conn);
            cmd.CommandType = CommandType.StoredProcedure;

            //se especifica el parámetro (nombre, valor que recibirá)
            SqlParameter param = new SqlParameter("@FirstName", "");
            param.SqlDbType = SqlDbType.NVarChar; //tipo de dato
            param.Size = 50; //caracteres máximos que acepta el parámetro

            //se agrega el parámetro al comando
            cmd.Parameters.Add(param);

            //USO DEL DATAREADER
            SqlDataReader dataReader = cmd.ExecuteReader();
            while (dataReader.Read())
            {
                personas.Add(new Persona
                {
                    PersonID = Convert.ToInt32(dataReader["PersonID"]),
                    LastName = dataReader["LastName"].ToString(),
                    FirstName = dataReader["FirstName"].ToString(),
                    HireDate = dataReader["HireDate"].ToString(),
                    EnrollmentDate = dataReader["EnrollmentDate"].ToString(),
                });
            }

            //cierra la conexión
            conn.Close();

            //asigna los datos a la tabla
            dgvPeople.ItemsSource = personas;
        }
    }
}
