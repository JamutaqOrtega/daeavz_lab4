﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAE_Avz_lab04_WPF
{
    internal class Persona
    {
        public int PersonID { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string HireDate { get; set; }
        public string EnrollmentDate { get; set; }
    }
}
