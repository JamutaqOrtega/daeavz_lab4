﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace DAE_Avz_lab04
{
    public partial class frmPersona : Form
    {
        SqlConnection conn;

        public frmPersona(SqlConnection conn)
        {
            this.conn = conn;
            InitializeComponent();
        }

        private void btnListar_Click(object sender, EventArgs e)
        {
            if (conn.State == ConnectionState.Open)
            {
                string sql = "SELECT * FROM People";
                SqlCommand cmd = new SqlCommand(sql, conn);
                SqlDataReader reader = cmd.ExecuteReader();

                DataTable dt = new DataTable();
                dt.Load(reader);
                dgvListado.DataSource = dt;
                dgvListado.Refresh();
            }
            else
            {
                MessageBox.Show("La conexión está cerrada");
            }
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            if (conn.State == ConnectionState.Open)
            {
                string nombre = txtNombre.Text.Trim();

                SqlCommand cmd = new SqlCommand("USP_BuscarPersonaNombre", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                SqlParameter param = new SqlParameter();
                param.ParameterName = "@FirstName";
                param.SqlDbType = SqlDbType.NVarChar;
                param.Value = nombre;

                cmd.Parameters.Add(param);

                SqlDataReader reader = cmd.ExecuteReader();
                DataTable dt = new DataTable();
                dt.Load(reader);
                dgvListado.DataSource = dt;
                dgvListado.Refresh();
            }
            else
            {
                MessageBox.Show("La conexión está cerrada");
            }
        }

        private void btnBuscar_dataReader_Click(object sender, EventArgs e)
        {
            //lista de Personas
            List<Persona> personas = new List<Persona>();

            if (conn.State == ConnectionState.Open) //si la conexión está abierta
            {
                //obtiene el texto a buscar
                string nombre = txtNombre.Text.Trim();

                //se crea un comando para hacer uso de procedimientos almacendos
                SqlCommand cmd = new SqlCommand("USP_BuscarPersonaNombre", conn);
                cmd.CommandType = CommandType.StoredProcedure;

                //se especifica el parámetro (nombre, valor que recibirá)
                SqlParameter param = new SqlParameter("@FirstName", nombre);
                param.SqlDbType = SqlDbType.NVarChar; //tipo de dato

                //se agrega el parámetro al comando
                cmd.Parameters.Add(param);

                //USO DEL DATAREADER
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    personas.Add(new Persona
                    {
                        PersonID = Convert.ToInt32(reader["PersonID"]),
                        LastName = Convert.ToString(reader["LastName"]),
                        FirstName = Convert.ToString(reader["FirstName"]),
                        HireDate = Convert.ToString(reader["HireDate"]),
                        EnrollmentDate = Convert.ToString(reader["EnrollmentDate"]),
                    });
                }

                //asigna los datos a la tabla
                dgvListado.DataSource = personas;
            }
            else
            {
                MessageBox.Show("La conexión está cerrada");
            }
        }
    }
}
